export const homeObjOne = {
    id: 'about',
    lightBg: false,
    lightText: true,
    topLine: "Projects",
    headline: "Yes",
    description: "Yes", 
    buttonLabel: 'Projects',
    imgStart: false,
    img: require('../../images/backend.svg'),
    alt: 'Projects',
    dark: true,
    primary: true,
    darkText: false
}