import React from 'react'
import { Button } from '../HeroSection/ButtonElements'
import { InfoContainer, InfoWrapper, InfoRow, Column1, TextWrapper, TopLine, Heading, Subtitle, BtnWrap} from './InfoElements'

const InfoSection = ({lightBg, id, imgStart, topLine, lightText, headline, darkText, description, buttonLabel}) => {
  return (
    <>
        <InfoContainer>
            <InfoWrapper>
                <InfoRow>
                    <Column1>
                        <TextWrapper>
                            <TopLine>PROJECTS</TopLine>
                            {/* <Heading>Yes</Heading>
                            <Subtitle>Yes</Subtitle>
                            <BtnWrap>
                                <Button to ='home'>YEs</Button>
                            </BtnWrap> */}
                        </TextWrapper>
                    </Column1>
                </InfoRow>
            </InfoWrapper>
        </InfoContainer>
    </>
  )
}

export default InfoSection