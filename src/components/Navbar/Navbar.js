import React, {useState, useEffect} from 'react'
import { Nav, NavbarContainer, NavLogo, MobileIcon, NavMenu, NavItem, NavLinks} from "./NavbarElements.js"
import logo from "../../images/logo1.png"
import {FaBars} from 'react-icons/fa'
import { Link } from 'react-scroll'

const Navbar = ({toggle}) => {
	const [scrollNav, setScrollNav] = useState(false)

	const changeNav = () => {
		if(window.scrollY >= 20 ) {
			setScrollNav(true)
		} else {
			setScrollNav(false)
		}
	}

	useEffect(() => {
		window.addEventListener('scroll', changeNav)
	}, [])

	

  return (
    <>
        <Nav scrollNav = {scrollNav}>
            <NavbarContainer>
                <NavLogo as = {Link} to = "home" smooth={true} duration={500}><img src={logo} alt="homelogo" style={{ width: 80, height: 80 }}/></NavLogo>
                <MobileIcon onClick={toggle}>
							<FaBars />
						</MobileIcon>
                <NavMenu>
                  <NavItem>
                    <NavLinks as = {Link} to = "home">About</NavLinks>
                  </NavItem>
                  <NavItem>
                    <NavLinks as = {Link} to = "projects">Projects</NavLinks>
                  </NavItem>
                  <NavItem>
                    <NavLinks as = {Link} to = "tools">Tools</NavLinks>
                  </NavItem>
                  <NavItem>
                    <NavLinks as = {Link} to = "connect">Connect</NavLinks>
                  </NavItem>
                </NavMenu>
            </NavbarContainer>
        </Nav>
    </>
  )
}

export default Navbar