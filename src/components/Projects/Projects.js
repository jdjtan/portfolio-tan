import React from 'react'
import API from "../../images/coursebooking.svg"
import Ecommerce from "../../images/e-commerce.png"
import WorkImg from "../../images/workImg.jpeg"
import ProductOrder from "../../images/product-order-API-postman.png"

const Projects = () => {
  return (
    <div name="projects" className='w-full md:h-screen bg-[#180d29] text-white pb-10'>
        <div className='max-w-[1000px] mx-auto p-4 flex flex-col justify-center w-full h-full'>
            <div className='pb-8'>
                <p className='text-4xl font-bold inline text-green-600'>Projects</p>
                <p className='pb-4 pt-2'>Check out some of my works</p>
            </div>
            
            {/* Container */}
            <div className='grid sm:grid-cols-2 md:grid-cols-3 gap-4'>

                {/* Grid Item */}
                <div style={{backgroundImage: `url(${API})`}} className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'>

                    {/* Hover Effects */}
                    <div className='opacity-5 group-hover:opacity-100'>
                        <span className='text-2xl font-bold tracking-wider'>
                            Course Booking API
                        </span>
                        <div className='pt-8 text-center'>
                            <a href="/">
                                <button className='text-center rounded-lg px-4 py-3 m-2 bg-white font-bold text-lg text-gray-300' disabled>Demo</button>
                            </a>
                            <a href="https://gitlab.com/b245-tan/s37-s41/course-booking">
                                <button className='text-center rounded-lg px-4 py-3 m-2 bg-white font-bold text-lg text-gray-700'>Code</button>
                            </a>
                        </div>
                    </div>
                </div>
                
                 {/* Grid Item */}
                <div style={{backgroundImage: `url(${ProductOrder})`}} className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'>

                    {/* Hover Effects */}
                    <div className='opacity-5 group-hover:opacity-100'>
                        <span className='text-2xl font-bold tracking-wider'>
                            Products Order API (with Add to Cart function)
                        </span>
                        <div className='pt-8 text-center'>
                            <a href="/">
                                <button className='text-center rounded-lg px-4 py-3 m-2 bg-white font-bold text-lg text-gray-300' disabled>Demo</button>
                            </a>
                            <a href="https://gitlab.com/b245-tan/capstone-tan/capstone-2-tan">
                                <button className='text-center rounded-lg px-4 py-3 m-2 bg-white font-bold text-lg text-gray-700'>Code</button>
                            </a>
                        </div>
                    </div>
                </div>

                {/* Grid Item */}
                <div style={{backgroundImage: `url(${Ecommerce})`}} className='shadow-lg shadow-[#040c16] group container rounded-md flex justify-center items-center mx-auto content-div'>

                    {/* Hover Effects */}
                    <div className='opacity-5 group-hover:opacity-100'>
                        <span className='text-2xl font-bold tracking-wider'>
                            Full Stack E-Commerce Website
                        </span>
                        <div className='pt-8 text-center'>
                            <a href="https://ecommerce-beta-six-77.vercel.app/">
                                <button className='text-center rounded-lg px-4 py-3 m-2 bg-white font-bold text-lg text-gray-700'>Demo</button>
                            </a>
                            <a href="https://gitlab.com/jdjtan/ecommerce">
                                <button className='text-center rounded-lg px-4 py-3 m-2 bg-white font-bold text-lg text-gray-700'>Code</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Projects