import React, {useState} from 'react'
import Video from '../../videos/video.mp4'
import {HeroContainer, HeroBg, VideoBg, HeroContent, HeroSub, HeroH1, HeroH2, HeroP, HeroBtnWrapper, ArrowForward, ArrowRight} from './HeroElements'
import { Button } from './ButtonElements'
import { Link } from 'react-scroll'

const HeroSection = () => {
	const [hover, setHover] = useState(false)

	const onHover = () => {
		setHover(!hover)
	}

	return(
		<HeroContainer name="home">
			<HeroBg>
				<VideoBg autoPlay loop muted src={Video} type="video/mp4"/>
			</HeroBg>
			<HeroContent>
				<HeroSub>Hi! My name is</HeroSub>
				<HeroH1>Jan Tan</HeroH1>
				<HeroH2>Full Stack Developer</HeroH2>
				<HeroP>
					I'm a full-stack developer specializing in building (and occasionally designing) digital experiences. Currently, I'm focused on building responsive full-stack websites and applications. 
				</HeroP>
				<HeroBtnWrapper>
					<Button as = {Link} to = "projects" onMouseEnter={onHover} onMouseLeave={onHover}>
						View Projects {hover ? <ArrowForward /> : <ArrowRight/>}
					</Button>
				</HeroBtnWrapper>
			</HeroContent>
		</HeroContainer>

	)
}

export default HeroSection