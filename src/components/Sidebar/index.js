import React from 'react'
import {SidebarContainer, Icon, CloseIcon, SidebarWrapper, SidebarMenu, SidebarLink} from './SidebarElements.js'
import { NavLink } from 'react-router-dom'

const Sidebar = ({isOpen, toggle}) => {
  return (
    <SidebarContainer isOpen={isOpen} onClick={toggle}>
			<Icon onClick={toggle}>
				<CloseIcon />
			</Icon>
			<SidebarWrapper>
				<SidebarMenu>
					<SidebarLink as = {NavLink} to = "/" onClick={toggle}>
					About
					</SidebarLink>
					<SidebarLink as = {NavLink} to = "/" onClick={toggle}>
					Projects
					</SidebarLink>
					<SidebarLink as = {NavLink} to = "/" onClick={toggle}>
					Tools
					</SidebarLink>
                    <SidebarLink as = {NavLink} to = "/" onClick={toggle}>
					Connect
					</SidebarLink>
				</SidebarMenu>
			</SidebarWrapper>
		</SidebarContainer>
  )
}

export default Sidebar