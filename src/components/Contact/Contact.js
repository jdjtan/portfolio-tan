import React from 'react'

const Contact = () => {
  return (
    <div name="connect" className='w-full h-screen bg-[#180d29] text-white flex justify-center items-center p-4'>
        <form method="POST" action = "https://getform.io/f/0061921c-d568-456b-85d6-302c3b9f968b" className='flex flex-col max-w-[600px] w-full'>
            <div className='pb-8'>
                <p className='text-4xl font-bold inline text-green-600'>Connect</p>
                <p className='py-4'>Submit the form below to send me a message!</p>
            </div>
            <input className='bg-[#ccd6f6] text-gray-700 p-2' type="text" placeholder='Name' name='name' />
            <input className='my-4 p-2 bg-[#ccd6f6] text-gray-700' type="email" placeholder='Email' name="email" />
            <textarea className='bg-[#ccd6f6] text-gray-700 p-2' name="message" rows="10" placeholder='Message'></textarea>
            <button className='text-white border-2 bg-[#7719a8] hover:bg-green-700 hover:border-green-600 mx-auto flex items-center my-8 px-4 py-3 '>Submit</button>
        </form>
    </div>
  )
}

export default Contact