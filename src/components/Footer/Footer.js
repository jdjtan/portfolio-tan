import React from 'react'
import { FaLinkedin} from 'react-icons/fa'
import {FooterContainer, FooterWrap, SocialMedia, SocialMediaWrap, SocialLogo, WebsiteRights, SocialIcons, SocialIconLink} from './FooterElements.js'
import logo from "../../images/logo1.png"


const Footer = () => {
	return(
		<FooterContainer>
			<FooterWrap>
				<SocialMedia>
					<SocialMediaWrap>
						<SocialLogo>
                            <img src={logo} alt="footerlogo" style={{ width: 80, height: 80 }}/>
						</SocialLogo>
						<WebsiteRights>Jan © {new Date().getFullYear()} All rights reseved.</WebsiteRights>
						<SocialIcons>
							<SocialIconLink href="//www.linkedin.com/in/jdjtan/" target="_blank" aria-label="Linkedin">
							<FaLinkedin />
							</SocialIconLink>
						</SocialIcons>
					</SocialMediaWrap>
				</SocialMedia>
			</FooterWrap>
		</FooterContainer>
	)
}

export default Footer