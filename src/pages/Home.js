import React, {useState} from 'react'
import HeroSection from '../components/HeroSection'

import Navbar from '../components/Navbar/Navbar'
import Sidebar from '../components/Sidebar'
import Tools from '../components/Tools/Tools'
import Projects from '../components/Projects/Projects'
import Contact from '../components/Contact/Contact'
import Footer from '../components/Footer/Footer'

function Home() {
    const [isOpen, setIsOpen] = useState(false)

    const toggle = () => {
        setIsOpen(!isOpen)
    }

  return (
    <>
      <Sidebar isOpen={isOpen} toggle={toggle} />
      <Navbar toggle={toggle}/>
      <HeroSection/>
      <Projects />
      {/* <InfoSection {...homeObjOne}/> */}
      <Tools/>
      <Contact />
      <Footer />
    </>
  )
}

export default Home